require('dotenv').config();
const firebase = require('firebase-admin');
const { sendSuccessMail } = require('./email.util');
const dummyUserData = require('./dummyUserData');

// firebase credentials
const serviceAccount = require(`./${process.env.FIREBASE_CRED_FILE}`);

// create firebase connection
firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: process.env.FIREBASE_DB_URL
});

// Load firebase database and create collection references
const db = firebase.database();
const usersRef = db.ref("/users");
const userInformationRef = db.ref("/userInformation");

// push new data in users collection
usersRef.push(dummyUserData);

// listen to child_added event on users collection,
// push it's data to userInformation collection and
// send success email to the email address
usersRef.once("child_added", async function (snapshot) {
  const data = snapshot.val();
  userInformationRef.push(data);

  // Send Email
  try {
    await sendSuccessMail(data.email, data.username);
    console.log('Email sent successfully!');
  } catch (err) {
    throw new Error(err);
  }
});