## Steps to configure and run this application
- Install App dependencies "npm install"
- Change User Data to submit on database in "dummyUserData.js" file.
- run app "npm start"
- Check inbox of the email you submitted in the "dummyUserData.js" file. If email address is valid then you should get a success mail. (email status is also logged on console log)

## Miscellaneous
- Firebase and Sendgrid credentials are saved in ".env" file