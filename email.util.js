const nodemailer = require('nodemailer');

let transporter = nodemailer.createTransport({
  service: 'SendGrid', // no need to set host or port etc.
  auth: {
    user: process.env.SENDGRID_USER,
    pass: process.env.SENDGRID_PASS
  }
});

function sendEmail(mailOptions) {
  // send mail with defined transport object
  return transporter.sendMail(mailOptions);
}

function sendSuccessMail(to, username) {
  const mailOptions = {
    from: '"Ravi Jagga" <no-reply@ravijagga.com>',
    to,
    subject: 'Welcome to our app',
    text: `Dear ${username}, \n\nWelcome to our app.`,
    html: `<p>Dear ${username}</p>
          <p>Welcome to our app.</p>`
  };

  // send email and return Promise object
  return sendEmail(mailOptions);
}

module.exports = { sendEmail, sendSuccessMail };